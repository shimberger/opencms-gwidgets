
import org.opencms.widgets.*;
import org.opencms.file.*;
import java.io.*;

public class GroovyExampleWidget extends CmsSelectWidget {

  @Override
  protected List<CmsSelectWidgetOption> parseSelectOptions(
        CmsObject cms,
        I_CmsWidgetDialog widgetDialog,
        I_CmsWidgetParameter param) {
	if (getSelectOptions() == null) {
	  def props = new Properties()  
	  def options = []
	  props.load(new ByteArrayInputStream(cms.readFile(getConfiguration()).getContents()))
	  props.each() { key, value -> options.add(new CmsSelectWidgetOption(key,false,value)) };
      setSelectOptions(options);
	}
	return getSelectOptions();
  }

}