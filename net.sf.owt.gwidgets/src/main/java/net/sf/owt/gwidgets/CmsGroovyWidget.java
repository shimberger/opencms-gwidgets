package net.sf.owt.gwidgets;


import groovy.lang.GroovyClassLoader;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.opencms.file.CmsFile;
import org.opencms.file.CmsObject;
import org.opencms.file.CmsResource;
import org.opencms.file.CmsResourceFilter;
import org.opencms.main.CmsException;
import org.opencms.widgets.I_CmsWidget;
import org.opencms.widgets.I_CmsWidgetDialog;
import org.opencms.widgets.I_CmsWidgetParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CmsGroovyWidget implements I_CmsWidget {

    private static final String GROOVY_SUFFIX = ".groovy";

    private static final String SYSTEM_MODULES = "/system/modules";

    private static final String WIDGETS_SUBFOLDER = "widgets";

    private static Logger LOG = LoggerFactory.getLogger(CmsGroovyWidget.class);

    private final GroovyClassLoader gcl = new GroovyClassLoader();
    
    private transient I_CmsWidget widget;
    
    private String configuration;
    
    public CmsGroovyWidget() {
        super();
    }
    
    private synchronized void initWidgetIfNecessary(CmsObject cms) {
        if (widget == null) {
            gcl.clearCache();
            widget = createGroovyWidget(cms);
        }
    }

    public I_CmsWidget newInstance() {
        return new CmsGroovyWidget();
    }

    public String getConfiguration() {
        return configuration;
    }
    
    public void setConfiguration(String configuration) {
        this.configuration = configuration;
    }

    public void setEditorValue(CmsObject cms, Map<String, String[]> formParameters, I_CmsWidgetDialog widgetDialog, I_CmsWidgetParameter param) {
        initWidgetIfNecessary(cms);
        widget.setEditorValue(cms, formParameters, widgetDialog, param);
    }
    
    public String getDialogWidget(CmsObject cms, I_CmsWidgetDialog widgetDialog, I_CmsWidgetParameter param) {
        initWidgetIfNecessary(cms);
        return widget.getDialogWidget(cms, widgetDialog, param);
    }

    public String getDialogHtmlEnd(CmsObject cms, I_CmsWidgetDialog widgetDialog, I_CmsWidgetParameter param) {
        initWidgetIfNecessary(cms);
        return widget.getDialogHtmlEnd(cms, widgetDialog, param);
    }

    public String getDialogIncludes(CmsObject cms, I_CmsWidgetDialog widgetDialog) {
        initWidgetIfNecessary(cms);
        return widget.getDialogIncludes(cms, widgetDialog);
    }

    public String getDialogInitCall(CmsObject cms, I_CmsWidgetDialog widgetDialog) {
        initWidgetIfNecessary(cms);
        return widget.getDialogInitCall(cms, widgetDialog);
    }

    public String getDialogInitMethod(CmsObject cms, I_CmsWidgetDialog widgetDialog) {
        initWidgetIfNecessary(cms);
        return widget.getDialogInitMethod(cms, widgetDialog);
    }

    public String getHelpBubble(CmsObject cms, I_CmsWidgetDialog widgetDialog, I_CmsWidgetParameter param) {
        initWidgetIfNecessary(cms);
        return widget.getHelpBubble(cms, widgetDialog, param);
    }

    public String getHelpText(I_CmsWidgetDialog widgetDialog, I_CmsWidgetParameter value) {
        if (widget == null) {
            throw new NullPointerException("widget was null when calling getHelpText()");
        }
        return widget.getHelpText(widgetDialog, value);
    }

    public String getWidgetStringValue(CmsObject cms, I_CmsWidgetDialog widgetDialog, I_CmsWidgetParameter param) {
        initWidgetIfNecessary(cms);
        return widget.getWidgetStringValue(cms, widgetDialog, param);
    }

    private Map<String, CmsResource> getWidgets(CmsObject cmso) throws CmsException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Looking for groovy widgets");
        }
        Map<String, CmsResource> widgets = new HashMap<String, CmsResource>();
        List<CmsResource> moduleFolders = cmso.readResources(SYSTEM_MODULES, CmsResourceFilter.DEFAULT.addRequireFolder(), false);
        for (CmsResource moduleFolder : moduleFolders) {
            String collectorFolderPath = moduleFolder.getRootPath() + WIDGETS_SUBFOLDER;
            if (LOG.isDebugEnabled()) {
                LOG.debug("Checking " + collectorFolderPath + " for groovy widgets");
            }
            if (!cmso.existsResource(collectorFolderPath)) {
                continue;
            }
            List<CmsResource> widgetrResources = cmso.readResources(collectorFolderPath, CmsResourceFilter.DEFAULT.addRequireFile(), true);
            for (CmsResource widgetResource : widgetrResources) {
                if (widgetResource.getName().endsWith(GROOVY_SUFFIX)) {
                    String filename = widgetResource.getName();
                    String basename = filename.substring(0, filename.lastIndexOf('.'));
                    widgets.put(basename, widgetResource);
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Found groovy widget in " + collectorFolderPath + ". Name is " + basename + " and path is "
                                + widgetResource.getRootPath() + "");
                    }
                }
            }
        }
        return widgets;
    }
    
    private I_CmsWidget createGroovyWidget(CmsObject cms) {
        I_CmsWidget widget = null;
        try {
            final Map<String,CmsResource> widgets = getWidgets(cms);
            final String widgetName = getGroovyWidgetName();
            final String widgetConf = getGroovyConfiguration();
            if (widgets.containsKey(widgetName)) {
                CmsFile groovyFile = cms.readFile(widgets.get(widgetName));
                @SuppressWarnings({ "deprecation", "rawtypes" })
                Class clazz = gcl.parseClass(new ByteArrayInputStream(groovyFile.getContents()), groovyFile.getRootPath());
                widget = (I_CmsWidget) clazz.newInstance();
                widget.setConfiguration(widgetConf);
            }
        } catch (Exception e) {
            if (LOG.isErrorEnabled()) {
                LOG.error("Error while creating widget",e);
            }
        }   
        return widget;
    }
    
    private String getGroovyWidgetName() {
        final String config = getConfiguration();
        int firstPipeIndex = config.indexOf("|");
        if (firstPipeIndex > 0) {
            return config.substring(0,firstPipeIndex);
        }
        return config.trim();
    }
    
    private String getGroovyConfiguration() {
        final String config = getConfiguration();
        int firstPipeIndex = config.indexOf("|");
        if (firstPipeIndex <= 0) {
            return "";
        }       
        return config.substring(firstPipeIndex+1);
    }

}
